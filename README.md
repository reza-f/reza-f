## About Me

Software Development, Cybersecurity, Cloud Computing & DevOps is my main interest. Currently I'm a Full Stack Developer and DevOps with application development and deployment responsibilities.

Familiar with:
- Web Frontend: Angular, Vue.
- Web Backend: PHP, NodeJS, Java, Python.
- Mobile: Flutter.
- Cloud Platform: AWS, GCP.
- Tools: Jenkins, Docker, SonarQube.

###### 🌚 I'm a Nocturn and Loving Cats 🐈.

[![Familiar With](https://skillicons.dev/icons?i=angular,vue,php,js,nodejs,java,python,dart,docker)](https://skillicons.dev)
